﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {
    private const int HEIGHT = 40;//38 + 2, top and bottom rows are in roof/cieling 
    private const float BLOCK_SIZE = 0.5f;
    private const float FLOOR_LEVEL = -9.75f;
	// Use this for initialization
	void Start () {
        for (int i = 0; i < HEIGHT; i++) {
            Instantiate(Resources.Load("1x2 " + (i % 2 == 0 ? "base ru" : "point ld")), new Vector3(-5 + i * BLOCK_SIZE, FLOOR_LEVEL + i * BLOCK_SIZE, 0), Quaternion.identity);
            Instantiate(Resources.Load("1x2 " + (i % 2 == 0 ? "base ld" : "point ru")), new Vector3(- 5 + (1 + i) * BLOCK_SIZE, FLOOR_LEVEL + i * BLOCK_SIZE, 0), Quaternion.identity);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
