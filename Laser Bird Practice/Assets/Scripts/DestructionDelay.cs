﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructionDelay : MonoBehaviour {
    [SerializeField]private float duration = 1;
    private float time = 0;
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if (time >= duration) {
            Destroy(gameObject);
        }
	}
}
