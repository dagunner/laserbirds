﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Math = System.Math;

public class Player : MonoBehaviour {
    [SerializeField]private Rigidbody2D rb;
    private const float jumpVelocity = 10;
    private const float terminal = 30;
    private const float rotationalLerpSpeed = 10;
    private const float topAngle = 30;
    private const float roof = 11;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Movement();
        if (Input.GetKeyDown(KeyCode.LeftShift)) {
            Instantiate(Resources.Load("Bullet"), transform.position, transform.rotation);
        }
	}


    private void Movement() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            rb.velocity = Vector3.up * jumpVelocity;
        }
        if (transform.position.y > roof) {
            rb.velocity = Vector3.zero;
        }
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, GetAngle(rb.velocity.y))), Time.deltaTime * rotationalLerpSpeed);
        
        //Debug.Log(rb.velocity.y + " " + transform.rotation.eulerAngles.z);
    }
    private float GetAngle(float x) {
        if (x >= 0) {
            return (float)(topAngle - topAngle * Math.Exp( -x * Math.Log(topAngle) / jumpVelocity));
        } else {
            return (float)(-90 + 90 * Math.Exp(x * Math.Log(90) / terminal));
        }
    }
}
