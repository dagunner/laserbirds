﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSegment : MonoBehaviour {
    enum Types : int {BOX = 0, CORNER_LD, CORNER_LU, CORNER_RD, CORNER_RU, H_POINT_LD, H_POINT_LU, H_POINT_RD, H_POINT_RU, H_BASE_LD, H_BASE_LU, H_BASE_RD, H_BASE_RU,
                      V_POINT_LD, V_POINT_LU, V_POINT_RD, V_POINT_RU, V_BASE_LD, V_BASE_LU, V_BASE_RD, V_BASE_RU };

    MapSegment[][] adjacent = { new MapSegment[4], new MapSegment[4], new MapSegment[4], new MapSegment[4]};
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
