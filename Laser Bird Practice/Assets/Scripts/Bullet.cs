﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Math = System.Math;

public class Bullet : MonoBehaviour {
    public const float RADIUS = 1.2f;
    public const float DURATION = 3;
    public const float SPEED = 25;
    private const float TRACKING_RANGE = 7;
    private const float TRACKING_AGGRESSION = 2;

    [SerializeField]private int damage = 5;
    [SerializeField]private Rigidbody2D rb;
    private float time = DURATION;
    private float destructionDelay = 0;
    private bool hit = false;
	// Use this for initialization
	void Start () {
        float angle = transform.rotation.eulerAngles.z;
        transform.position += new Vector3((float)Math.Cos(angle * Math.PI / 180), (float)Math.Sin(angle * Math.PI / 180), 0);
    }
	
	// Update is called once per frame
	void Update () {
        //targeting
        GameObject target = null;
        float closestDistance = TRACKING_RANGE;
        foreach (GameObject enemy in Enemy.GetEnemies()) {
            float distance = (transform.position - enemy.transform.position).magnitude;
            if (distance < closestDistance) {
                closestDistance = distance;
                target = enemy;
            }
        }
        if (target != null) {
            //calculating the amount the bullet needs to turn.
            Vector3 displacement = target.transform.position - transform.position;
            float targetAngle = (float)(Math.Atan(displacement.y / displacement.x) * 180 / Math.PI);
            float currentAngle = transform.rotation.eulerAngles.z;
            if (currentAngle > 180) {
                currentAngle -= 360;
            } else if (currentAngle < -180) {
                currentAngle += 360;
            }
            float dispAngle = targetAngle - currentAngle;
            if (dispAngle > 180) {
                dispAngle -= 360;
            } else if (dispAngle < -180) {
                dispAngle += 360;
            }
            //turning the bullet
            if (Math.Abs(dispAngle) <= TRACKING_AGGRESSION) {
                transform.rotation = Quaternion.Euler(0, 0, targetAngle);
            } else {
                transform.rotation = Quaternion.Euler(0, 0, currentAngle + TRACKING_AGGRESSION * Math.Sign(dispAngle));
            }

        }

        //movement
        float angle = transform.rotation.eulerAngles.z;
        rb.velocity = new Vector3((float)Math.Cos(angle * Math.PI / 180), (float)Math.Sin(angle * Math.PI / 180), 0) * SPEED;

        //delayed destruction
        if (hit) {
            destructionDelay -= Time.deltaTime;
            if (destructionDelay <= 0) {
                Instantiate(Resources.Load("Explosion"), transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }

        //timeout
        time -= Time.deltaTime;
        if (time < 0)Destroy(gameObject);
    }
    public void OnTriggerEnter2D(Collider2D collision) {
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag == "Map") {
            Hit();
        } else if (collision.gameObject.tag == "Enemy") {
            collision.gameObject.GetComponent<Enemy>().Hit(damage);
            Hit();
        }
    }

    public void Hit() {
        hit = true;
        destructionDelay = RADIUS / (SPEED + 1);
    }
}
