﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    private const float PERIOD = 1.5f;
    private const float GRAVITY = -4.9f;
    private static List<GameObject> enemies = new List<GameObject>();

    [SerializeField]private Rigidbody2D rb;
    private int health = 20;
    private float baseY;
    private float t = 0;
	// Use this for initialization
	void Start () {
        baseY = transform.position.y;
        enemies.Add(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
        float vel = - GRAVITY * PERIOD;
        t += Time.deltaTime;
        if (health > 0) {
            t %= PERIOD;
        }
        transform.position = new Vector3(transform.position.x + (health > 0? 0:Time.deltaTime), baseY + vel * t + GRAVITY * t * t);
        
	}
    

    public void OnCollisionEnter2D(Collision2D collision) {
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag == "Map" && health <= 0) {
            Destroy(gameObject);
        }
    }

    public void Hit(int damage) {
        health -= damage;
        if (health <= 0) {
            enemies.Remove(gameObject);
        }
    }

    public static List<GameObject> GetEnemies() {
        return enemies;
    }
}
